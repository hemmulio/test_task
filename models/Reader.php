<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reader".
 *
 * @property integer $id
 * @property string $name
 * @property string $time_create
 * @property string $time_update
 *
 * @property ReaderBook[] $readerBooks
 * @property Book[] $books
 */
class Reader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reader';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'time_create', 'time_update'], 'required'],
            [['time_create', 'time_update'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'time_create' => 'Time Create',
            'time_update' => 'Time Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReaderBooks()
    {
        return $this->hasMany(ReaderBook::className(), ['reader_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['id' => 'book_id'])->viaTable('reader_book', ['reader_id' => 'id']);
    }
}
