<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=test_task',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
